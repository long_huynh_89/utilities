package com.utilities.lib.adapter.decoration

import android.graphics.Rect
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import java.io.Serializable

class SpaceItemDecoration private constructor(): RecyclerView.ItemDecoration() {

    private var edgeLeft      = 0
    private var edgeRight     = 0
    private var edgeTop       = 0
    private var edgeBottom    = 0
    private var dividerLeft   = 0
    private var dividerRight  = 0
    private var dividerTop    = 0
    private var dividerBottom = 0
    private var viewTypes     : ArrayList<Int>? = null

    override fun getItemOffsets(
            outRect: Rect, view: View, parent: RecyclerView,
            state: RecyclerView.State) {

        val position = parent.getChildAdapterPosition(view)
        if (position == RecyclerView.NO_POSITION) return

        val type = parent.adapter?.getItemViewType(position)
        if (type == null || (viewTypes?.isNotEmpty() == true
                        && viewTypes?.contains(type) == false)) return

        val layoutManager = parent.layoutManager

        when (layoutManager) {
            is GridLayoutManager -> {
                view.let { v ->
                    getGridItemOffsets(layoutManager,
                            layoutManager.spanCount, v, parent, outRect)
                }
            }

            is LinearLayoutManager -> {
                view.let { v ->
                    getLinearItemOffsets(layoutManager, v, parent, outRect)
                }
            }

            null -> {}
            else -> throw RuntimeException("Not Support: Unknown Layout Manager")
        }
    }

    private fun getLinearItemOffsets(
            layoutManager: LinearLayoutManager,
            view: View, parent: RecyclerView, outRect: Rect?) {
        if (layoutManager.reverseLayout) {
            throw RuntimeException("Not Support: reverseLayout (true)")
        }

        val position = parent.getChildAdapterPosition(view)
        val count = parent.adapter?.itemCount ?: return

        when (layoutManager.orientation) {
            LinearLayoutManager.VERTICAL -> {
                outRect?.top = if (position == 0) edgeTop else dividerTop
                outRect?.bottom = if (position == count - 1) edgeBottom else dividerBottom

                outRect?.left = edgeLeft
                outRect?.right = edgeRight
            }
            LinearLayoutManager.HORIZONTAL -> {
                outRect?.left = if (position == 0) edgeLeft else dividerLeft
                outRect?.right = if (position == count - 1) edgeRight else dividerRight

                outRect?.top = edgeTop
                outRect?.bottom = edgeBottom
            }
            else -> throw RuntimeException("Not Support: Unknown Orientation")
        }
    }

    private fun getGridItemOffsets(
            layoutManager: GridLayoutManager, spanCount: Int,
            view: View, parent: RecyclerView, outRect: Rect?) {
        if (layoutManager.reverseLayout) {
            throw RuntimeException("Not Support: reverseLayout (true)")
        }

        val position = parent.getChildAdapterPosition(view)
        val itemCount = parent.adapter?.itemCount ?: return

        var spanIndex = position % spanCount
        var spanGroupIndex = position / spanCount
        var spanSize = 1
        var isLatestGroup = true

        layoutManager.spanSizeLookup?.also { lookup ->
            spanSize = lookup.getSpanSize(position)
            spanIndex = lookup.getSpanIndex(position, spanCount)
            spanGroupIndex = lookup.getSpanGroupIndex(position, spanCount)

            (position..(itemCount - 1)).forEach { i ->
                val index = lookup.getSpanGroupIndex(i, spanCount)

                if (index != spanGroupIndex) {
                    isLatestGroup = false
                    return@forEach
                }
            }
        } ?:let {
            var spanGroupCount = itemCount / spanCount

            if (itemCount % spanCount > 0) {
                spanGroupCount += 1
            }

            if (spanGroupIndex != spanGroupCount) {
                isLatestGroup = false
            }
        }

        // Nếu VERTICAL:
        //  - spanIndex: Column Index
        //  - spanGroupIndex: Row Index
        //
        // Nếu HORIZONTAL:
        //  - spanIndex: Row Index
        //  - spanGroupIndex: Column Index

        when (layoutManager.orientation) {
            GridLayoutManager.VERTICAL -> {
                outRect?.left = if (spanIndex == 0) edgeLeft else dividerLeft
                outRect?.right = if (spanIndex + spanSize ==
                        spanCount) edgeRight else dividerRight
                outRect?.top = if (spanGroupIndex == 0) edgeTop else dividerTop
                outRect?.bottom = if (isLatestGroup) edgeBottom else dividerBottom
            }
            GridLayoutManager.HORIZONTAL -> {
                throw RuntimeException("Not Support: HORIZONTAL Orientation")
            }
            else -> throw RuntimeException("Not Support: Unknown Orientation")
        }
    }

    data class Builder(
            var edgeLeft      :Int = 0,
            var edgeRight     :Int = 0,
            var edgeTop       :Int = 0,
            var edgeBottom    :Int = 0,

            var dividerLeft   :Int = 0,
            var dividerRight  :Int = 0,
            var dividerTop    :Int = 0,
            var dividerBottom :Int = 0
    ): Serializable {

        private val viewTypesValue = ArrayList<Int>()
        val viewTypes: List<Int> get() = viewTypesValue

        fun setDividerSize(size: Int, includeEdge: Boolean = true): Builder {
            if (includeEdge) {
                edgeLeft = size
                edgeRight = size
                edgeTop = size
                edgeBottom = size
            } else {
                edgeLeft = 0
                edgeRight = 0
                edgeTop = 0
                edgeBottom = 0
            }

            dividerTop = size / 2
            dividerBottom = size / 2
            dividerLeft = size / 2
            dividerRight = size / 2

            return this
        }

        fun setDividerHeight(size: Int) {
            dividerTop = size / 2
            dividerBottom = size / 2
        }

        fun setEdge(size: Int): Builder {
            edgeLeft = size
            edgeRight = size
            edgeTop = size
            edgeBottom = size

            return this
        }

        fun addViewType(type: Int): Builder {
            if (!viewTypesValue.contains(type)) {
                viewTypesValue.add(type)
            }
            return this
        }

        fun build(): RecyclerView.ItemDecoration {
            return SpaceItemDecoration().also {
                it.edgeLeft = edgeLeft
                it.edgeRight = edgeRight
                it.edgeTop = edgeTop
                it.edgeBottom = edgeBottom
                it.dividerTop = dividerTop
                it.dividerBottom = dividerBottom
                it.dividerLeft = dividerLeft
                it.dividerRight = dividerRight
                it.viewTypes = viewTypesValue
            }
        }
    }
}