package com.utilities.lib.adapter.fragment

import androidx.recyclerview.widget.RecyclerView
import android.view.View

class BaseViewHolder(view : View): RecyclerView.ViewHolder(view)