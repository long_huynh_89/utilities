package com.utilities.lib.adapter.fragment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.utilities.lib.R
import java.lang.ref.WeakReference

abstract class FragmentAdapter<VH: androidx.recyclerview.widget.RecyclerView.ViewHolder>
private constructor(
        fragmentManagerInstance: FragmentManager,
        inflaterInstance: LayoutInflater
): androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder>() {

    constructor(activity: FragmentActivity):
            this(activity.supportFragmentManager, activity.layoutInflater)
    constructor(fragment: Fragment):
            this(fragment.childFragmentManager, fragment.layoutInflater)

    private val fragmentManager = WeakReference(fragmentManagerInstance)
    private val inflater = WeakReference(inflaterInstance)

    final override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int
    ): BaseViewHolder {
        return BaseViewHolder(inflater.get()?.inflate(
                R.layout.asdasdasdasdwdewqd, parent, false)!!)
    }

    final override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        fragmentManager.get()?.let { _ ->

        }
    }
}