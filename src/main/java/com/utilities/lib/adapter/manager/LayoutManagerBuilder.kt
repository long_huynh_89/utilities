package com.utilities.lib.adapter.manager

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.Serializable

data class LayoutManagerBuilder(
        private val type: Type,
        private val orientation: Int,
        private val reverseLayout: Boolean,
        private val spanCount: Int = 0
): Serializable {

    fun build(context: Context?): androidx.recyclerview.widget.RecyclerView.LayoutManager {
        return when (type) {
            Type.LinearLayoutManager -> {
                LinearLayoutManager(context, orientation, reverseLayout)
            }
            Type.GridLayoutManager -> {
                GridLayoutManager(context, spanCount, orientation, reverseLayout)
            }
            Type.FlexboxLayoutManager -> TODO("NOT IMPLEMENT")
        }
    }

    enum class Type {
        LinearLayoutManager,
        GridLayoutManager,
        FlexboxLayoutManager
    }
}