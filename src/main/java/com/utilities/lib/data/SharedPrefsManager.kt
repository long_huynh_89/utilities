package com.utilities.lib.data

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.CallSuper
import com.google.common.base.Optional
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.ObservableEmitter

open class SharedPrefsManager(
        context: Context, name: String,
        private val listenForChange: Boolean = true) {

    private val sharedPreferences = context
            .getSharedPreferences(name, Context.MODE_PRIVATE)

    private val listener = Listener()

    private val emitters by lazy {
        ArrayList<DataEmitter>()
    }

    protected val gson by lazy {
        Gson()
    }

    init {
        if (listenForChange) {
            sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
        }
    }

    protected fun observe(key: String, type: Type): Observable<Optional<Any>> {
        val stackTrace = Thread.currentThread().stackTrace

        return Observable.create { emitter ->
            check(key, type, stackTrace)

            val first = getValue(key, type)

            if (!emitter.isDisposed) {
                emitter.onNext(Optional
                        .fromNullable(first))
                add(key, type, emitter)
            }
        }
    }

    private fun check(key: String, type: Type, stackTrace: Array<StackTraceElement>) {
        synchronized(emitters, {
            var list: ArrayList<DataEmitter>? = null

            emitters.forEach {
                if (it.emitter.isDisposed) {
                    (list ?: ArrayList<DataEmitter>().apply {
                        list = this
                    }).add(it)
                }
                else if (it.key == key && it.type != type) {
                    throw RuntimeException().apply {
                        this.stackTrace = stackTrace
                    }
                }
            }

            list?.forEach {
                emitters.remove(it)
            }
        })
    }

    private fun add(key: String, type: Type, emitter: ObservableEmitter<Optional<Any>>) {
        synchronized(emitters, {
            emitters.add(DataEmitter(key, type, emitter))
        })
    }

    private fun remove(emitter: DataEmitter) {
        synchronized(emitters, {
            emitters.remove(emitter)
        })
    }

    protected fun <T> fromJson(value: String, clazz: Class<T>): T? {
        return try {
            gson.fromJson(value, clazz)
        } catch (e: Exception) {
            null
        }
    }

    protected fun <T> toJson(value: T): String {
        return gson.toJson(value)
    }

    protected fun getValue(key: String, type: Type, defValue: Any? = null): Any? {
        return when (type) {
            Type.String -> sharedPreferences.getString(
                    key, defValue as? String)
            Type.Long -> {
                if (sharedPreferences.contains(key)) {
                    sharedPreferences.getLong(key,
                            (defValue as? Long) ?: 0L)
                } else null
            }
            Type.Boolean -> {
                if (sharedPreferences.contains(key)) {
                    sharedPreferences.getBoolean(key,
                            (defValue as? Boolean) ?: false)
                } else null
            }
        }
    }

    protected fun putString(key: String, data: String?) {
        sharedPreferences.edit().putString(key, data).apply()
    }

    protected fun putLong(key: String, data: Long?) {
        data?.let {
            sharedPreferences.edit().putLong(key, it).apply()
        } ?:let {
            sharedPreferences.edit().remove(key).apply()
        }
    }

    protected fun putBoolean(key: String, data: Boolean?) {
        data?.let {
            sharedPreferences.edit().putBoolean(key, it).apply()
        } ?:let {
            sharedPreferences.edit().remove(key).apply()
        }
    }

    protected fun edit(): SharedPreferences.Editor {
        return sharedPreferences.edit()
    }

    @CallSuper
    open fun shutdown() {
        if (listenForChange) {
            sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener)
        }
    }

    private inner class Listener : SharedPreferences.OnSharedPreferenceChangeListener {
        override fun onSharedPreferenceChanged(
                sharedPreferences: SharedPreferences?, key: String?) {
            var type = Type.String

            val list = emitters.filter {
                (it.key == key).apply {
                    if (this) {
                        type = it.type
                    }
                }
            }

            if (list.isNotEmpty() && key != null) {
                val next = getValue(key, type)

                list.forEach {
                    if (it.emitter.isDisposed) {
                        remove(it)
                    } else {
                        it.emitter.onNext(Optional
                                .fromNullable(next))
                    }
                }
            }
        }
    }

    private data class DataEmitter(
            val key: String, val type: Type,
            val emitter: ObservableEmitter<Optional<Any>>
    )

    enum class Type {
        String,
        Long,
        Boolean
    }
}