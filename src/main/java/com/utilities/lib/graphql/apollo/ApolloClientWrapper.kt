package com.utilities.lib.graphql.apollo

import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Mutation
import com.apollographql.apollo.api.Operation
import com.apollographql.apollo.api.Query
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import io.reactivex.Single
import io.reactivex.SingleEmitter
import java.util.concurrent.Callable

open class ApolloClientWrapper(
        private val client: ApolloClient) {

    open fun <D: Operation.Data, T, V: Operation.Variables>
            enqueue(mutation: Mutation<D, T, V>): Single<Response<T>> {
        return enqueue(mutation, null)
    }

    private fun <D: Operation.Data, T, V: Operation.Variables> enqueue(
            mutation: Mutation<D, T, V>,
            prevStackTraces: Array<StackTraceElement>?): Single<Response<T>> {

        val single = Single.create<Response<T>> {
            client.mutate(mutation).enqueue(RequestCallback(it)) }

        val stackTraces = prevStackTraces
                ?: Thread.currentThread().stackTrace

        return single
                .map { checkResponse(it) }
                .onErrorResumeNext {
                    onError(it, stackTraces, Callable {
                        enqueue(mutation, stackTraces)
                    })
                }
    }

    open fun <D: Operation.Data, T, V: Operation.Variables>
            enqueue(query: Query<D, T, V>): Single<Response<T>> {
        return enqueue(query, null)
    }

    private fun <D: Operation.Data, T, V: Operation.Variables> enqueue(
            query: Query<D, T, V>,
            prevStackTraces: Array<StackTraceElement>?): Single<Response<T>> {
        val single = Single.create<Response<T>> { emitter ->
            client.query(query).enqueue(
                    RequestCallback(emitter))
        }
        val stackTraces = prevStackTraces ?: Thread.currentThread().stackTrace

        return single.map { response -> checkResponse(response) }
                .onErrorResumeNext { e ->
                    onError(e, stackTraces, Callable {
                        enqueue(query, stackTraces)
                    })
                }
    }

    protected open fun <T> onError(
            e: Throwable, stackTraces: Array<StackTraceElement>,
            retry: Callable<Single<T>>): Single<T> {
        e.stackTrace = stackTraces
        return Single.error(e)
    }

    protected open fun <T> checkResponse(
            response: Response<T>): Response<T> {
        return response
    }

    private class RequestCallback<T>(
            private val emitter: SingleEmitter<Response<T>>):
            ApolloCall.Callback<T>() {

        override fun onFailure(e: ApolloException) {
            if (!emitter.isDisposed) {
                emitter.onError(e)
            }
        }

        override fun onResponse(response: Response<T>) {
            if (!emitter.isDisposed) {
                emitter.onSuccess(response)
            }
        }
    }
}