package com.utilities.lib.image

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.AsyncTask
import com.facebook.common.internal.Supplier
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.datasource.DataSources
import com.facebook.drawee.backends.pipeline.DraweeConfig
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilderSupplier
import com.facebook.drawee.interfaces.SimpleDraweeControllerBuilder
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory
import com.facebook.imagepipeline.core.ExecutorSupplier
import com.facebook.imagepipeline.core.ImagePipeline
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.common.base.Optional
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import java.util.concurrent.Executor
import com.facebook.drawee.backends.pipeline.Fresco as Base

object Fresco {

    private val schedulerAPI by lazy {
        Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR)
    }
    
    @JvmField
    internal var DEBUG = false

    @JvmField
    internal val TAG = "Fresco"

    @JvmStatic
    val paint by lazy {
        Paint().apply {
            color = Color.BLACK
            textSize = 50f
        }
    }

    @JvmStatic
    val hasBeenInitialized: Boolean
        get() = Base.hasBeenInitialized()

    private fun downloadPrivate(uri: Uri): Single<Any> {
        return Single.fromCallable {
            val request = ImageRequestBuilder
                    .newBuilderWithSource(uri).build()
            val source  = getImagePipeline()
                    .prefetchToDiskCache(request, null)

            Optional.fromNullable(DataSources.waitForFinalResult(source))
        }
    }

    fun download(uri: Uri): Single<Any> {
        return downloadPrivate(uri)
                .subscribeOn(schedulerAPI)
    }

    fun initialize(context: Context) {
        initialize(context, null)
    }

    fun initialize(context: Context, imagePipelineConfig: ImagePipelineConfig?) {
        initialize(context, imagePipelineConfig, null)
    }

    fun initialize(context: Context, imagePipelineConfig: ImagePipelineConfig?, draweeConfig: DraweeConfig?) {
        Base.initialize(context, imagePipelineConfig, draweeConfig)

        val supplier = PipelineDraweeControllerBuilderSupplier(context, null)
        initialize(supplier)
    }

    fun initialize(context: Context, okHttpClient: OkHttpClient) {
        val config = OkHttpImagePipelineConfigFactory
            .newBuilder(context, okHttpClient)
            .setDownsampleEnabled(true)
            .setExecutorSupplier(object : ExecutorSupplier {
                override fun forLocalStorageRead(): Executor {
                    return AsyncTask.THREAD_POOL_EXECUTOR
                }
                override fun forDecode(): Executor {
                    return AsyncTask.THREAD_POOL_EXECUTOR
                }
                override fun forLocalStorageWrite(): Executor {
                    return AsyncTask.THREAD_POOL_EXECUTOR
                }
                override fun forLightweightBackgroundTasks(): Executor {
                    return AsyncTask.THREAD_POOL_EXECUTOR
                }
                override fun forBackgroundTasks(): Executor {
                    return AsyncTask.THREAD_POOL_EXECUTOR
                }
            })
            .build()

        initialize(context, config)
    }

    fun createBitmap(width: Int, height: Int, config: Bitmap.Config): CloseableReference<Bitmap> {
        return Base.getImagePipelineFactory().platformBitmapFactory.createBitmap(width, height, config)
    }

    fun createBitmap(width: Int, height: Int): CloseableReference<Bitmap> {
        return createBitmap(width, height, Bitmap.Config.RGB_565)
    }

    fun getImagePipeline(): ImagePipeline {
        return Base.getImagePipeline()
    }

    private fun initialize(supplier: PipelineDraweeControllerBuilderSupplier) {
        sDraweeControllerBuilderSupplier = supplier
    }

    fun shutDown() {
        sDraweeControllerBuilderSupplier = null
    }

    fun getSimpleDraweeControllerBuilder(): SimpleDraweeControllerBuilder? {
        return sDraweeControllerBuilderSupplier?.get() as? SimpleDraweeControllerBuilder
    }

    fun newDraweeControllerBuilder(): PipelineDraweeControllerBuilder {
        return Base.newDraweeControllerBuilder()
    }

    fun fetchEncodedImage(imageRequest: ImageRequest, callerContext: Any?)
            : DataSource<CloseableReference<CloseableImage>> {
        return Base.getImagePipeline().fetchDecodedImage(imageRequest, callerContext)
    }

    private var sDraweeControllerBuilderSupplier: Supplier<*>? = null
}