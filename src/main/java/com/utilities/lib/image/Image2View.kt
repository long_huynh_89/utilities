package com.utilities.lib.image

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Animatable
import android.net.Uri
import androidx.core.app.ActivityCompat
import android.util.AttributeSet
import com.facebook.common.internal.Supplier
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.datasource.IncreasingQualityDataSourceSupplier
import com.facebook.drawee.controller.ControllerListener
import com.facebook.drawee.generic.GenericDraweeHierarchy
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.utilities.lib.R
import java.lang.ref.WeakReference
import android.widget.ImageView as Base

open class Image2View : SimpleDraweeView {

    private var isInit = false
    private val selfWeak by lazy {
        WeakReference(this)
    }
    private val editModeDrawable by lazy {
        ActivityCompat.getDrawable(context, R.mipmap.place_holder_in_edit_mode)
    }

    constructor(context: Context?, hierarchy: GenericDraweeHierarchy?)
            : super(context, hierarchy) {
        initView()
    }

    constructor(context: Context?) : this(context, null as AttributeSet?)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyle: Int) : super(context, attrs, defStyle) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initView()
    }

    protected open fun preInitView() {}

    private fun initView() {
        preInitView()
        isInit = true
    }

    fun setImageURI(thumbnail: String?, image: String?) {
        processImageURI(null, arrayOf(thumbnail, image))
    }

    fun setImageURI(thumbnail: String?, image: String?, origin: String?) {
        processImageURI(null, arrayOf(thumbnail, image, origin))
    }

    /**
     * Small Image ... Large Image
     */
    private fun processImageURI(listener: ControllerListener<ImageInfo>?,
                                images: Array<out String?>) {
        val callerContext = null
        val uriArray = images.reversed().map {
            try { Uri.parse(it) } catch (e: Exception) { null }
        }

        controller = Fresco
                .newDraweeControllerBuilder()
                .setCallerContext(callerContext)
                .also {
                    val images = uriArray.map { MySupplier(it, callerContext) }
                    val supplier = IncreasingQualityDataSourceSupplier.create(images)
                    it.dataSourceSupplier = supplier
                }
                .also {
                    if (isInit) {
                        it.controllerListener = ControllerListenerWrapper(listener, selfWeak)
                    }
                }
                .setOldController(controller)
                .build()
    }

    /**
     * Small Image ... Large Image
     */
    fun setImageURI(listener: ControllerListener<ImageInfo>?,
                    vararg requestList: ImageRequest?) {
        val callerContext = null
        val uriArray = requestList.reversed().filterNotNull()

        controller = Fresco
                .newDraweeControllerBuilder()
                .setCallerContext(callerContext)
                .also {
                    val images = uriArray.map { MySupplier(it, callerContext) }
                    val supplier = IncreasingQualityDataSourceSupplier.create(images)
                    it.setDataSourceSupplier(supplier)
                }
                .also {
                    if (isInit) {
                        it.setControllerListener(
                                ControllerListenerWrapper(listener, selfWeak))
                    }
                }
                .setOldController(controller)
                .build()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (isInEditMode) {
            editModeDrawable?.draw(canvas)
        }
    }

    override fun setImageURI(uri: Uri?, callerContext: Any?) {
        controller = Fresco
                .newDraweeControllerBuilder()
                .setCallerContext(callerContext)
                .setUri(uri)
                .also {
                    if (isInit) {
                        it.setControllerListener(
                                ControllerListenerWrapper(null, selfWeak))
                    }
                }
                .setOldController(controller)
                .build()
    }

    protected open fun onFailure(id: String?, throwable: Throwable?) {}

    protected open fun onRelease(id: String?) {}

    protected open fun onSubmit(id: String?, callerContext: Any?) {}

    protected open fun onIntermediateImageSet(id: String?, imageInfo: ImageInfo?) {}

    protected open fun onIntermediateImageFailed(id: String?, throwable: Throwable?) {}

    protected open fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, animatable: Animatable?) {}

    private class MySupplier(
            private val imageRequest: ImageRequest,
            private val callerContext: Any?
    ): Supplier<DataSource<CloseableReference<CloseableImage>>> {

        constructor(uri: Uri?, callerContext: Any?): this(ImageRequestBuilder
                .newBuilderWithSource(uri).build(), callerContext)

        override fun get(): DataSource<CloseableReference<CloseableImage>> {
            return Fresco.fetchEncodedImage(imageRequest, callerContext)
        }
    }

    private class ControllerListenerWrapper(
            private val listener: ControllerListener<ImageInfo>?,
            private val reference: WeakReference<Image2View>
    ): ControllerListener<ImageInfo> {

        override fun onFailure(id: String?, throwable: Throwable?) {
            listener?.onFailure(id, throwable)
            reference.get()?.onFailure(id, throwable)
        }

        override fun onRelease(id: String?) {
            listener?.onRelease(id)
            reference.get()?.onRelease(id)
        }

        override fun onSubmit(id: String?, callerContext: Any?) {
            listener?.onSubmit(id, callerContext)
            reference.get()?.onSubmit(id, callerContext)
        }

        override fun onIntermediateImageSet(id: String?, imageInfo: ImageInfo?) {
            listener?.onIntermediateImageSet(id, imageInfo)
            reference.get()?.onIntermediateImageSet(id, imageInfo)
        }

        override fun onIntermediateImageFailed(id: String?, throwable: Throwable?) {
            listener?.onIntermediateImageFailed(id, throwable)
            reference.get()?.onIntermediateImageFailed(id, throwable)
        }

        override fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, animatable: Animatable?) {
            listener?.onFinalImageSet(id, imageInfo, animatable)
            reference.get()?.onFinalImageSet(id, imageInfo, animatable)
        }
    }
}