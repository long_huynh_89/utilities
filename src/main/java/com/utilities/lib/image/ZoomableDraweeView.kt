package com.utilities.lib.image

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.drawable.Animatable
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import com.facebook.imagepipeline.image.ImageInfo
import com.utilities.lib.image.zoomable.AnimatedZoomableController
import com.utilities.lib.image.zoomable.DoubleTapGestureListener
import com.utilities.lib.image.zoomable.GestureListenerWrapper
import com.utilities.lib.image.zoomable.ZoomableController

open class ZoomableDraweeView : Image2View {

    private val mImageBounds = RectF()
    private val mViewBounds = RectF()

    lateinit var zoomableController: ZoomableController
    var allowTouchInterceptionWhileZoomed = true

    private lateinit var mTapGestureDetector: GestureDetector
    private lateinit var mZoomableListener: ZoomableController.Listener
    private lateinit var mTapListenerWrapper: GestureListenerWrapper

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyle: Int) : super(context, attrs, defStyle)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun preInitView() {
        mZoomableListener = object : ZoomableController.Listener {
            override fun onTransformBegin(transform: Matrix) {}

            override fun onTransformChanged(transform: Matrix) {
                this@ZoomableDraweeView.onTransformChanged(transform)
            }

            override fun onTransformEnd(transform: Matrix) {}
        }

        zoomableController = createZoomableController()
        zoomableController.setListener(mZoomableListener)

        mTapListenerWrapper = GestureListenerWrapper()
        mTapGestureDetector = GestureDetector(context, mTapListenerWrapper)

        setTapListener(DoubleTapGestureListener(this))
    }

    override fun onFinalImageSet(
            id: String?, imageInfo: ImageInfo?,
            animatable: Animatable?) {
        onFinalImageSet()
    }

    override fun onRelease(id: String?) {
        onRelease()
    }

    protected open fun createZoomableController(): ZoomableController {
        return AnimatedZoomableController.newInstance()
    }

    protected open fun getImageBounds(outBounds: RectF) {
        hierarchy?.getActualImageBounds(outBounds)
    }

    protected open fun getLimitBounds(outBounds: RectF) {
        outBounds.set(0f, 0f, width.toFloat(), height.toFloat())
    }

    fun setTapListener(tapListener: GestureDetector.SimpleOnGestureListener) {
        mTapListenerWrapper.setListener(tapListener)
    }

    fun setIsLongpressEnabled(enabled: Boolean) {
        mTapGestureDetector.setIsLongpressEnabled(enabled)
    }

    override fun onDraw(c: Canvas?) {
        c?.let { canvas ->
            val saveCount = canvas.save()
            canvas.concat(zoomableController.transform)

            try {
                super.onDraw(canvas)
            } catch (e: Exception) {
                if (Fresco.DEBUG) {
                    Log.e(Fresco.TAG, "[ZoomableDraweeView] [onDraw] $e", e)
                }
            }

            canvas.restoreToCount(saveCount)
        }
    }

    fun canScrollVerticallyInZoomMode(direction: Int): Boolean {
        return zoomableController.canScrollVertically(direction)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(e: MotionEvent?): Boolean {
        e?.let { event ->
            if (mTapGestureDetector.onTouchEvent(event)) {
                return true
            }

            if (zoomableController.onTouchEvent(event)) {
                if (!allowTouchInterceptionWhileZoomed && !zoomableController.isIdentity()) {
                    parent.requestDisallowInterceptTouchEvent(true)
                }
                return true
            }
            
            if (super.onTouchEvent(event)) {
                return true
            }
            
            // None of our components reported that they handled the touch event. Upon returning false
            // from this method, our parent won't send us any more events for this gesture. Unfortunately,
            // some components may have started a delayed action, such as a long-press timer, and since we
            // won't receive an ACTION_UP that would cancel that timer, a false event may be triggered.
            // To prevent that we explicitly send one last cancel event when returning false.
            val cancelEvent = MotionEvent.obtain(event)
            cancelEvent.action = MotionEvent.ACTION_CANCEL
            mTapGestureDetector.onTouchEvent(cancelEvent)
            zoomableController.onTouchEvent(cancelEvent)
            cancelEvent.recycle()
        }
        return false
    }

    override fun computeHorizontalScrollRange(): Int {
        return zoomableController.computeHorizontalScrollRange()
    }

    override fun computeHorizontalScrollOffset(): Int {
        return zoomableController.computeHorizontalScrollOffset()
    }

    override fun computeHorizontalScrollExtent(): Int {
        return zoomableController.computeHorizontalScrollExtent()
    }

    override fun computeVerticalScrollRange(): Int {
        return zoomableController.computeVerticalScrollRange()
    }

    override fun computeVerticalScrollOffset(): Int {
        return zoomableController.computeVerticalScrollOffset()
    }

    override fun computeVerticalScrollExtent(): Int {
        return zoomableController.computeVerticalScrollExtent()
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        updateZoomableControllerBounds()
    }

    private fun onFinalImageSet() {
        if (!zoomableController.isEnabled) {
            zoomableController.isEnabled = true
            updateZoomableControllerBounds()
        }
    }

    private fun onRelease() {
        zoomableController.isEnabled = false
    }

    protected open fun onTransformChanged(transform: Matrix) {
        invalidate()
    }

    protected open fun updateZoomableControllerBounds() {
        getImageBounds(mImageBounds)
        getLimitBounds(mViewBounds)
        zoomableController.setImageBounds(mImageBounds)
        zoomableController.setViewBounds(mViewBounds)
    }

    companion object {
        private const val HUGE_IMAGE_SCALE_FACTOR_THRESHOLD = 1.1f
    }
}