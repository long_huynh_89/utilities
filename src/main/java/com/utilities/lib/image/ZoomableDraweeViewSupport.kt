package com.utilities.lib.image

import android.content.Context
import android.util.AttributeSet
import com.utilities.lib.image.zoomable.AnimatedZoomableControllerSupport
import com.utilities.lib.image.zoomable.ZoomableController

class ZoomableDraweeViewSupport : ZoomableDraweeView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyle: Int) : super(context, attrs, defStyle)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun createZoomableController(): ZoomableController {
        return AnimatedZoomableControllerSupport.newInstance()
    }
}