package com.utilities.lib.locale

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import java.util.*

abstract class LocaleManager {

    protected abstract fun getLanguage(context: Context?): String?
    protected abstract fun setLanguage(context: Context?, newLanguage: String)

    /**
     * Sử dụng function này để tạo Context Wrapper trong
     * - android.app.Application.attachBaseContext (Optional)
     * - android.app.Activity.attachBaseContext
     * - android.app.Service.attachBaseContext
     *
     * Sử dụng để update locale trong
     * - android.app.Application.onConfigurationChanged
     */
    fun updateLocale(context: Context?): Context? {
        return updateResources(context, getLanguage(context))
    }

    /**
     * Sử dụng để update Language trong Settings, ...
     */
    fun updateLocale(context: Context?, newLanguage: String): Context? {
        setLanguage(context, newLanguage)
        return updateResources(context, newLanguage)
    }

    @Suppress("DEPRECATION")
    protected fun updateResources(context: Context?, language: String?,
                                  isSave: Boolean = true): Context? {
        if (context == null) return null

        val locale = Locale(language)

        if (isSave && Locale.getDefault().
                        language != language) {
            Locale.setDefault(locale)
        }

        val res = context.resources
        val config = Configuration(res.configuration)

        return if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale)
            context.createConfigurationContext(config)
        } else {
            config.locale = locale
            res.updateConfiguration(config, res.displayMetrics)

            context
        }
    }
}
