package com.utilities.lib.locale

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.Button
import com.utilities.lib.R

class LocalizedButton : Button {

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        context?.let { ctx ->
            attrs?.let {
                initView(ctx, it)
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        context?.let { ctx ->
            attrs?.let {
                initView(ctx, it)
            }
        }
    }

    @SuppressLint("CustomViewStyleable")
    private fun initView(context: Context, attrs: AttributeSet) {
        val typedArray = context.obtainStyledAttributes(
                attrs, R.styleable.LocalizedTextView, 0, 0)
        val textId = typedArray.getResourceId(
                R.styleable.LocalizedTextView_text, 0)
        if (textId == 0) {
            val text = typedArray.getText(
                    R.styleable.LocalizedTextView_text)
            if (text?.isNotEmpty() == true) {
                setText(text)
            }
        }
        typedArray.recycle()

        if (textId != 0) {
            setText(textId)
        }
    }
}
