package com.utilities.lib.video

import android.net.Uri
import java.io.Serializable

data class VideoData(
        val type: VideoType = VideoType.YouTube,
        val apiKey: String? = null,
        val id: String? = null,
        val url: String? = null
): Serializable {

    fun getYouTubeId(): String {
        return (id ?: Uri.parse(url).getQueryParameter("v"))!!
    }

    fun checkYouTube() {
        checkNotNullAndEmpty(apiKey, "API Key Is Invalid.")
        checkNotNullAndEmpty(getYouTubeId(), "YouTube Video Id Is Invalid.")
    }

    fun checkDirectLink() {
        checkNotNullAndEmpty(url, "Video Url Is Invalid.")
    }

    companion object {
        internal val None = VideoData()

        private fun checkNotNullAndEmpty(input: String?, msg: String) {
            if (input?.isNotEmpty() != true) {
                throw RuntimeException(msg)
            }
        }
    }
}