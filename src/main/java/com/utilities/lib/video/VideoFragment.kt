package com.utilities.lib.video

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.utilities.lib.BuildConfig
import com.utilities.lib.R

@Suppress("CAST_NEVER_SUCCEEDS")
class VideoFragment : Fragment() {

    private var dataValue: VideoData? = null
    private var player: VideoPlayer? = null

    var data: VideoData?
        get() {
            return dataValue ?: arguments
                ?.getSerializable(VIDEO_DATA) as? VideoData
        }
        set(value) {
            if (value != this.data) {
                dataValue = value ?: VideoData.None
                arguments?.putSerializable(VIDEO_DATA, dataValue)
                initVideoPlayer()
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val context = this.context ?: return null
        val layout = VideoFrameLayout(context)

        layout.id = R.id.videoFragmentContainer

        return layout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val width = arguments?.getInt(WIDTH)
        val height = arguments?.getInt(HEIGHT)
        val params = view.layoutParams

        if (width != null && height != null
            && (params.width  != width
            ||  params.height != height)) {

            params.width = width
            params.height = height

            view.layoutParams = params
        }

        if (data !== VideoData.None) {
            setupVideo()
        }
    }

    private fun initVideoPlayer() {
        releaseVideo()

        if (data !== VideoData.None) {
            if (DEBUG) {
                Log.i(TAG, "[VideoFragment] [initVideoPlayer] oldFragment: ....?")
            }

            setupVideo()
        } else {
            val oldFragment = childFragmentManager
                .findFragmentById(R.id.videoFragmentContainer)

            if (DEBUG) {
                Log.i(TAG, "[VideoFragment] [initVideoPlayer] oldFragment: $oldFragment")
            }

            oldFragment?.let {
                childFragmentManager
                    .beginTransaction()
                    .remove(oldFragment).commit()
            }
        }
    }

    private fun releaseVideo() {
        if (DEBUG) {
            Log.i(TAG, "[VideoFragment] [releaseVideo] player: $player")
        }

        player?.release()
        player = null
    }

    private fun setupVideo() {
        val videoData = this.data ?: return
        val fragment: Fragment = when (videoData.type) {
            VideoType.YouTube -> {
                videoData.checkYouTube()

                YouTubePlayerSupportFragment
                    .newInstance() as Fragment
            }
            else -> TODO("Not Implement")
        }

        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.videoFragmentContainer, fragment)
        transaction.commit()

        when (fragment) {
            is YouTubePlayerSupportFragment -> {
                fragment.initialize(
                    videoData
                        .apiKey, OnInitializedListener()
                )
            }
            else -> TODO("Not Implement")
        }

        if (DEBUG) {
            Log.i(TAG, "[VideoFragment] [setupVideo] fragment: $fragment")
        }
    }

    override fun onResume() {
        if (DEBUG) {
            Log.i(TAG, "[VideoFragment] [onResume] player: $player")
        }

        super.onResume()
        player?.onResume()
    }

    override fun onPause() {
        if (DEBUG) {
            Log.i(TAG, "[VideoFragment] [onPause] player: $player")
        }

        player?.onPause()
        super.onPause()
    }

    private inner class OnInitializedListener : YouTubePlayer.OnInitializedListener {

        override fun onInitializationSuccess(
            provider: YouTubePlayer.Provider?,
            player: YouTubePlayer?, wasRestored: Boolean
        ) {
            if (DEBUG) {
                Log.i(
                    TAG, "[VideoFragment] [OnInitializedListener] " +
                            "[onInitializationSuccess] wasRestored: $wasRestored"
                )
            }

            data?.let {
                this@VideoFragment.player =
                    VideoPlayer(player, it)
            }
        }

        override fun onInitializationFailure(
            provider: YouTubePlayer.Provider?,
            error: YouTubeInitializationResult?
        ) {
            if (DEBUG) {
                error?.let {
                    Log.e(
                        TAG, "[VideoFragment] [OnInitializedListener] " +
                                "[onInitializationFailure] Error: $error, " +
                                "isUserRecoverableError: ${error.isUserRecoverableError}"
                    )
                }
            }

            if (error?.isUserRecoverableError == true) {
                // TODO Resolve Error
            }
        }
    }

    companion object {
        const val VIDEO_DATA = "VIDEO_DATA"
        const val WIDTH = "WIDTH"
        const val HEIGHT = "HEIGHT"

        var DEBUG = BuildConfig.DEBUG
        var TAG = "YouTubeAndroidPlayerAPI"
    }
}
