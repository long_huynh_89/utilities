package com.utilities.lib.video

import android.os.Handler
import android.util.Log
import com.google.android.exoplayer2.Player
import com.google.android.youtube.player.YouTubePlayer

internal class VideoPlayer(
    player: YouTubePlayer?, videoData: VideoData
) {

    private val handler = Handler()
    private var youTubePlayer: YouTubePlayer? = player?.also {
        // Load và play youtube video
        it.loadVideo(videoData.getYouTubeId())

        // Ẩn các controls mặc định của youtube
        // it.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS)

        it.setPlaybackEventListener(PlaybackEventListener())
        it.setPlayerStateChangeListener(PlayerStateChangeListener())
        it.setOnFullscreenListener { isFullScreen = it }
    }
    private val onResumeRunnable: Runnable
    private val repeatMode = "${Player.REPEAT_MODE_ALL}".toInt()

    var isFullScreen = false

    init {
        onResumeRunnable = Runnable {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [onResumeRunnable]")
            }

            play()
        }
    }

    private fun play() {
        if (VideoFragment.DEBUG) {
            Log.i(VideoFragment.TAG, "[VideoPlayer] [play] youTubePlayer: $youTubePlayer")
        }

        youTubePlayer?.play()
    }

    fun release() {
        if (VideoFragment.DEBUG) {
            Log.i(VideoFragment.TAG, "[VideoPlayer] [release] youTubePlayer: $youTubePlayer")
        }

        youTubePlayer?.release()
        youTubePlayer = null
    }

    fun onResume() {
        handler.post(onResumeRunnable)
    }

    fun onPause() {
        handler.removeCallbacks(onResumeRunnable)
    }

    fun closeFullScreen() {
        youTubePlayer?.setFullscreen(false)
    }

    private inner class PlaybackEventListener : YouTubePlayer.PlaybackEventListener {
        override fun onSeekTo(newPositionMillis: Int) {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlaybackEventListener] " +
                        "[onSeekTo] newPositionMillis: $newPositionMillis")
            }
        }

        override fun onBuffering(isBuffering: Boolean) {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlaybackEventListener] " +
                        "[onBuffering] isBuffering: $isBuffering")
            }
        }

        override fun onPlaying() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlaybackEventListener] [onPlaying]")
            }
        }

        override fun onStopped() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlaybackEventListener] [onStopped]")
            }

            if (repeatMode == Player.REPEAT_MODE_ALL
                    || repeatMode == Player.REPEAT_MODE_ONE) {
                play()
            }
        }

        override fun onPaused() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlaybackEventListener] [onPaused]")
            }
        }
    }

    private inner class PlayerStateChangeListener : YouTubePlayer.PlayerStateChangeListener {
        override fun onAdStarted() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlayerStateChangeListener] [onAdStarted]")
            }
        }

        override fun onLoading() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlayerStateChangeListener] [onLoading]")
            }
        }

        override fun onVideoStarted() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlayerStateChangeListener] " +
                        "[onVideoStarted] duration: ${youTubePlayer?.durationMillis} ms")
            }
        }

        override fun onLoaded(videoId: String?) {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlayerStateChangeListener] [onLoaded] videoId: $videoId")
            }
        }

        override fun onVideoEnded() {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlayerStateChangeListener] " +
                        "[onVideoEnded] current: ${youTubePlayer?.currentTimeMillis} ms")
            }
        }

        override fun onError(reason: YouTubePlayer.ErrorReason?) {
            if (VideoFragment.DEBUG) {
                Log.i(VideoFragment.TAG, "[VideoPlayer] [PlayerStateChangeListener] [onError] reason: $reason")
            }
        }
    }
}