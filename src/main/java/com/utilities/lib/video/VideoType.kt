package com.utilities.lib.video

import java.io.Serializable

enum class VideoType: Serializable {
    YouTube,
    DirectLink
}