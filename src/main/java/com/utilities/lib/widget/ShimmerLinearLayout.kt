package com.utilities.lib.widget

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.widget.LinearLayout
import com.utilities.lib.image.Fresco

class ShimmerLinearLayout : LinearLayout, ShimmerHelper.Parent {

    private val helper: ShimmerHelper

    constructor(context: Context?) : super(context) {
        helper = if (Fresco.hasBeenInitialized) {
            ShimmerWithFrescoHelper(context, this)
        } else {
            ShimmerHelper(context, this)
        }
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        helper = when {
            (attrs != null && !Fresco.hasBeenInitialized) ->
                ShimmerHelper(context, this, attrs)
            (attrs != null && Fresco.hasBeenInitialized) ->
                ShimmerWithFrescoHelper(context, this, attrs)
            Fresco.hasBeenInitialized ->
                ShimmerWithFrescoHelper(context, this)
            else ->
                ShimmerHelper(context, this)
        }
    }
    constructor(context: Context?, attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        helper = when {
            (attrs != null && !Fresco.hasBeenInitialized) ->
                ShimmerHelper(context, this, attrs)
            (attrs != null && Fresco.hasBeenInitialized) ->
                ShimmerWithFrescoHelper(context, this, attrs)
            Fresco.hasBeenInitialized ->
                ShimmerWithFrescoHelper(context, this)
            else ->
                ShimmerHelper(context, this)
        }
    }

    override fun onDetachedFromWindow() {
        helper.onDetachedFromWindow()
        super.onDetachedFromWindow()
    }

    override fun dispatchDraw(canvas: Canvas?) {
        if (isInEditMode) {
            super.dispatchDraw(canvas)
        } else {
            helper.dispatchDraw(canvas)
        }
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        helper.setVisibility(visibility)
    }

    override fun superDispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
    }

    fun startShimmerAnimation() {
        helper.startShimmerAnimation()
    }

    fun stopShimmerAnimation() {
        helper.stopShimmerAnimation()
    }
}