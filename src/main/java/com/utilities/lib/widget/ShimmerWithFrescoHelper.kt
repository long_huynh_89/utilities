package com.utilities.lib.widget

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.ViewGroup
import com.facebook.common.references.CloseableReference
import com.utilities.lib.image.Fresco

internal class ShimmerWithFrescoHelper : ShimmerHelper {

    private var bitmapReference: CloseableReference<Bitmap>? = null

    constructor(context: Context?, parent: ViewGroup) : super(context, parent)
    constructor(context: Context?, parent: ViewGroup,
                attrs: AttributeSet) : super(context, parent, attrs)

    override fun releaseMaskBitmap() {
        CloseableReference.closeSafely(bitmapReference)
        bitmapReference = null
    }

    override fun getMaskBitmap(width: Int, height: Int): Bitmap {
        val bitmap = bitmapReference?.get()

        if (bitmap?.isRecycled != false) {
            val reference = Fresco.createBitmap(
                    width, height, Bitmap.Config.ALPHA_8)
            bitmapReference = reference
            return reference.get()
        }

        return bitmap!!
    }
}