package com.utilities.lib.widget

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.view.Window

class StatusBarView : View {

    private var statusBarHeight = 0

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initView(context)
    }

    private fun initView(context: Context?) {
        statusBarHeight = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.
                        KITKAT) getStatusBarHeight(context) else  0

        if (statusBarHeight <= 0) {
            visibility = View.GONE
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = MeasureSpec.makeMeasureSpec(
                statusBarHeight, MeasureSpec.EXACTLY)

        super.onMeasure(widthMeasureSpec, height)
    }

    override fun getVisibility(): Int {
        return if (statusBarHeight > 0) {
            super.getVisibility()
        } else {
            View.GONE
        }
    }

    companion object {
        private var height: Int? = null

        fun getStatusBarHeight(context: Context?): Int {
            if (height != null) return height!!

            var value = (context as? Activity)?.window?.let { window ->
                val rectangle = Rect()
                window.decorView.getWindowVisibleDisplayFrame(rectangle)

                val statusBarHeight = rectangle.top
                val contentViewTop = window.findViewById<
                        View>(Window.ID_ANDROID_CONTENT).top

                contentViewTop - statusBarHeight
            } ?: 0

            if (value == 0) {
                val resourceId = context?.resources?.getIdentifier(
                        "status_bar_height", "dimen", "android") ?: 0
                if (resourceId > 0) {
                    value = context?.resources?.
                            getDimensionPixelSize(resourceId) ?:0
                }
            }

            return Math.abs(value).also {
                if (it != 0) {
                    height = it
                }
            }
        }
    }
}